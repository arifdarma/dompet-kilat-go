FROM golang:1.17.6-alpine3.15

WORKDIR /usr/app

COPY . .

RUN go mod download

RUN go get github.com/cosmtrek/air