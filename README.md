# Dompet-kilat-go

How to run :<br>
open directory write command `docker-compose up` on terminal<br>
<br>
Port golang project : `8080`<br>
Port database : `3306` <br>
Database : Mysql <br>
Databases :
<ol>
    <li>
        Users : <br>
        `username varchar(255) <br>
        email varchar(255) <br>
        password varchar(255) <br>
        token varchar(255)`
    </li>
    <li>
        sbns : <br>
        `name varchar(255) <br>
        amount varchar(255) <br>
        tenor int <br>
        rate int <br>
        rate varchar(50)`
    </li>
    <li>
        reksadanas : <br>
        `name varchar(255) <br>
        amount varchar(255) <br>
        returns int`
    </li>
    <li>
        financings : <br>
        `name varchar(255)<br>
        count int <br>
        sub varchar(50)`
    </li>
    <li>
        productiveinvoices : <br>
        `name varchar(255) <br>
        amount varchar(255) <br>
        grade varchar(255) <br>
        rate int`
    </li>
    <li>
        conventionalosfs : <br>
        `name varchar(255) <br>
        amount varchar(255) <br>
        tenor int <br>
        grade varchar(255) <br>
        rate int`
    </li>
    <li>
        conventionalinvoices : <br>
        `name varchar(255) <br>
        amount varchar(255) <br>
        tenor int <br>
        grade varchar(255) <br>
        rate int`
    </li>
</ol>
List Endpoint :<br>
<ol>
    <li>
        `/register` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"username":"test", "email":"test@gmail.com","password":"test123"}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/login` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"email":"test@gmail.com","password":"test123"}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/createProdInvs` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"Name":"Testing ProdInv","Amount":"1000000","Grade":"A","Rate":8}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/createConvInvs` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"Name":"Testing ConvInv","Amount":"1000000","Tenor":100,"Grade":"A","Rate":8}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/createConvOsfs` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"Name":"Testing ConvOsf","Amount":"1000000","Tenor":100,"Grade":"A","Rate":8}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/createFinancings` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"Name":"Testing financing","Count":100,"Sub":"SBR"}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/createSbns` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"Name":"Testing sbn","Amount":"1000000","Tenor":100,"Rate":8,"Type":"SBR"}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/createReksadanas` : <br>
        Method : `POST`<br>
        Request Field : <br>
        <ul>
            <li>example :<br>
            {"Name":"Testing reksadana3","Amount":"1000000","Returns":1}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/getSbns` : <br>
        Method : `GET`<br>
        Response Field : <br>
        <ul>
            <li>example :<br>
            {
                "data": [
                    {
                        "Name": "SBRXXX",
                        "Amount": "1000000",
                        "Tenor": 120,
                        "Rate": 7,
                        "Type": "SBR"
                    },
                    {
                        "Name": "SBRYYY",
                        "Amount": "2000000",
                        "Tenor": 120,
                        "Rate": 8,
                        "Type": "SBR"
                    },
                    {
                        "Name": "Testing sbn",
                        "Amount": "1000000",
                        "Tenor": 100,
                        "Rate": 8,
                        "Type": "SBR"
                    }
                ]
            }<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/getReksadanas` : <br>
        Method : `GET`<br>
        Response Field : <br>
        <ul>
            <li>example :<br>
            {
                "data": [
                    {
                        "Name": "INB",
                        "Amount": "20000000",
                        "Returns": -1
                    },
                    {
                        "Name": "PT TELMOM",
                        "Amount": "10000000",
                        "Returns": 1
                    },
                    {
                        "Name": "Testing reksadana3",
                        "Amount": "1000000",
                        "Returns": 1
                    }
                ]
            }<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/getFinancings` : <br>
        Method : `GET`<br>
        Response Field : <br>
        <ul>
            <li>example :<br>
            {
                "data": [
                    {
                        "Name": "Invoice Financing",
                        "Count": 35,
                        "Sub": ""
                    },
                    {
                        "Name": "OSF Financing",
                        "Count": 25,
                        "Sub": ""
                    },
                    {
                        "Name": "SBN",
                        "Count": 10,
                        "Sub": ""
                    },
                    {
                        "Name": "Reksadana",
                        "Count": 20,
                        "Sub": ""
                    },
                    {
                        "Name": "Conventional Invoice",
                        "Count": 20,
                        "Sub": "invoice"
                    },
                    {
                        "Name": "Productive Invoice",
                        "Count": 15,
                        "Sub": "invoice"
                    },
                    {
                        "Name": "Conventional OSF",
                        "Count": 15,
                        "Sub": "osf"
                    },
                    {
                        "Name": "Productive OSF",
                        "Count": 10,
                        "Sub": "osf"
                    },
                    {
                        "Name": "Testing financing",
                        "Count": 100,
                        "Sub": "SBR"
                    }
                ]
            }<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/getProdInvs` : <br>
        Method : `GET`<br>
        Response Field : <br>
        <ul>
            <li>example :<br>
            {
    "data": [
        {
            "Name": "PT YJK",
            "Amount": "1000000",
            "Grade": "B",
            "Rate": 16
        },
        {
            "Name": "PT KKY",
            "Amount": "4000000",
            "Grade": "B+",
            "Rate": 14
        },
        {
            "Name": "Testing ProdInv",
            "Amount": "1000000",
            "Grade": "A",
            "Rate": 8
        }
    ]
}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/getConvInvs` : <br>
        Method : `GET`<br>
        Response Field : <br>
        <ul>
            <li>example :<br>
            {
    "data": [
        {
            "Name": "PT KKY",
            "Amount": "4000000",
            "Tenor": 120,
            "Grade": "B+",
            "Rate": 14
        },
        {
            "Name": "PT YJK",
            "Amount": "1000000",
            "Tenor": 120,
            "Grade": "B",
            "Rate": 16
        },
        {
            "Name": "Testing ConvInv",
            "Amount": "1000000",
            "Tenor": 100,
            "Grade": "A",
            "Rate": 8
        }
    ]
}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
    <li>
        `/getConvOsfs` : <br>
        Method : `GET`<br>
        Response Field : <br>
        <ul>
            <li>example :<br>
            {
    "data": [
        {
            "Name": "PT KKY",
            "Amount": "4000000",
            "Tenor": 120,
            "Grade": "B+",
            "Rate": 14
        },
        {
            "Name": "PT YJK",
            "Amount": "1000000",
            "Tenor": 120,
            "Grade": "B",
            "Rate": 16
        },
        {
            "Name": "Testing ConvOsf",
            "Amount": "1000000",
            "Tenor": 100,
            "Grade": "A",
            "Rate": 8
        }
    ]
}<br></li>
            <li>Type : json</li>
        </ul>
    </li>
</ol>