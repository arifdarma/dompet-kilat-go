package controller

import (
	"net/http"
	"simpleweb/classes"
	"simpleweb/service"

	"github.com/gin-gonic/gin"
)

type conventionalController struct {
	service service.ConventionalService
}

func NewConventionalsController(service service.ConventionalService) *conventionalController {
	return &conventionalController{service}
}

func (rc *conventionalController) FindAllConventionalOSFs(c *gin.Context) {
	convOsf, resmsg := rc.service.FindAllConventionalOSF()
	if resmsg != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": resmsg,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": convOsf,
	})

}

func (rc *conventionalController) FindAllConventionalInvoices(c *gin.Context) {
	convInv, resmsg := rc.service.FindAllConventionalInvoice()
	if resmsg != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": resmsg,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": convInv,
	})

}

func (rc *conventionalController) FindAllProductiveInvoices(c *gin.Context) {
	prodInv, resmsg := rc.service.FindAllProductiveInvoice()
	if resmsg != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": resmsg,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": prodInv,
	})

}

func (rc *conventionalController) CreateConventionalOsfs(c *gin.Context) {
	var convOsf classes.Conventionalosfs

	err := c.ShouldBindJSON(&convOsf)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	convOsfRes, err := rc.service.CreateConventionalOSF(convOsf)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"input":    convOsf,
		"Response": convOsfRes,
	})
}

func (rc *conventionalController) CreateConventionalInvoices(c *gin.Context) {
	var convInv classes.Conventionalinvoices

	err := c.ShouldBindJSON(&convInv)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	convInvRes, err := rc.service.CreateConventionalInvoice(convInv)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"input":    convInv,
		"Response": convInvRes,
	})
}

func (rc *conventionalController) CreateProductiveInvoices(c *gin.Context) {
	var prodInv classes.Productiveinvoices

	err := c.ShouldBindJSON(&prodInv)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	prodInvRes, err := rc.service.CreateProductiveInvoice(prodInv)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"input":    prodInv,
		"Response": prodInvRes,
	})
}
