package controller

import (
	"net/http"
	"simpleweb/classes"
	"simpleweb/service"

	"github.com/gin-gonic/gin"
)

type financingController struct {
	service service.FinancingService
}

func NewFinancingsController(service service.FinancingService) *financingController {
	return &financingController{service}
}

func (rc *financingController) FindAllFinancings(c *gin.Context) {
	financing, resmsg := rc.service.FindAll()
	if resmsg != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": resmsg,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": financing,
	})

}

func (rc *financingController) CreateFinancings(c *gin.Context) {
	var financing classes.Financings

	err := c.ShouldBindJSON(&financing)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	financingRes, err := rc.service.Create(financing)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"input":    financing,
		"Response": financingRes,
	})
}
