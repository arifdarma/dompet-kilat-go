package controller

import (
	"net/http"
	"simpleweb/classes"
	"simpleweb/service"

	"github.com/gin-gonic/gin"
)

type sbnController struct {
	service service.SbnService
}

func NewSbnsController(service service.SbnService) *sbnController {
	return &sbnController{service}
}

func (rc *sbnController) FindAllSbns(c *gin.Context) {
	sbn, resmsg := rc.service.FindAll()
	if resmsg != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": resmsg,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": sbn,
	})

}

func (rc *sbnController) CreateSbns(c *gin.Context) {
	var sbn classes.Sbns

	err := c.ShouldBindJSON(&sbn)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	sbnRes, err := rc.service.Create(sbn)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"input":    sbn,
		"Response": sbnRes,
	})
}
