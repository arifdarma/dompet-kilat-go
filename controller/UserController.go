package controller

import (
	"net/http"
	"simpleweb/classes"
	"simpleweb/service"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type userController struct {
	service service.UserService
}

func NewUsersController(service service.UserService) *userController {
	return &userController{service}
}

func (us *userController) Register(c *gin.Context) {
	var userReq classes.Userrequest

	err := c.ShouldBindJSON(&userReq)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(userReq.Password), 10)
	userReq.Password = string(bytes[:])
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}
	userRes, err := us.service.Register(classes.Users(userReq))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"respons": userRes,
	})

}

func (us *userController) Login(c *gin.Context) {
	var userReq classes.Userrequest
	err := c.ShouldBindJSON(&userReq)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}
	user, err := us.service.Login(userReq)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}
	match := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userReq.Password))
	if match == nil {
		key, err := us.service.GenerateJWT(userReq)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"user result login": "success",
				"error":             err,
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"user result login": "success",
				"key":               key,
			})
		}
	} else {
		c.JSON(http.StatusOK, gin.H{
			"user result login": match,
		})
	}

}
