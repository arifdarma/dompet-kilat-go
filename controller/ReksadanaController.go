package controller

import (
	"net/http"
	"simpleweb/classes"
	"simpleweb/service"

	"github.com/gin-gonic/gin"
)

type reksadanaController struct {
	service service.ReksadanaService
}

func NewReksadanasController(service service.ReksadanaService) *reksadanaController {
	return &reksadanaController{service}
}

func (rc *reksadanaController) CreateReksadanas(c *gin.Context) {
	var reksadana classes.Reksadanas

	err := c.ShouldBindJSON(&reksadana)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	reksadanaRes, err := rc.service.Create(reksadana)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors : ": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"input":    reksadana,
		"Response": reksadanaRes,
	})
}

func (rc *reksadanaController) FindAllReksadanas(c *gin.Context) {
	reksadana, resmsg := rc.service.FindAll()
	if resmsg != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": resmsg,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"data": reksadana,
	})

}
