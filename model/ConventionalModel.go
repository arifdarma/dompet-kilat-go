package model

import (
	"simpleweb/classes"

	"gorm.io/gorm"
)

type ConventionalModel interface {
	CreateConventionalOSF(convOsf classes.Conventionalosfs) (convOsfObj classes.Conventionalosfs, resmsg error)
	FindAllConventionalOSF() (convOsf []classes.Conventionalosfs, resmsg error)
	CreateConventionalInvoice(convInv classes.Conventionalinvoices) (convInvObj classes.Conventionalinvoices, resmsg error)
	FindAllConventionalInvoice() (convInv []classes.Conventionalinvoices, resmsg error)
	CreateProductiveInvoice(prodInv classes.Productiveinvoices) (prodInvObj classes.Productiveinvoices, resmsg error)
	FindAllProductiveInvoice() (prodInv []classes.Productiveinvoices, resmsg error)
}

type conventionalModel struct {
	db *gorm.DB
}

func NewConventionalsModel(db *gorm.DB) *conventionalModel {
	return &conventionalModel{db}
}

func (us *conventionalModel) CreateConventionalOSF(convOsf classes.Conventionalosfs) (convOsfObj classes.Conventionalosfs, resmsg error) {
	resmsg = us.db.Create(convOsf).Error
	return convOsfObj, resmsg
}

func (us *conventionalModel) FindAllConventionalOSF() (convOsf []classes.Conventionalosfs, resmsg error) {
	resmsg = us.db.Find(&convOsf).Error
	return convOsf, resmsg
}

func (us *conventionalModel) CreateConventionalInvoice(convInv classes.Conventionalinvoices) (convInvObj classes.Conventionalinvoices, resmsg error) {
	resmsg = us.db.Create(convInv).Error
	return convInvObj, resmsg
}

func (us *conventionalModel) FindAllConventionalInvoice() (convInv []classes.Conventionalinvoices, resmsg error) {
	resmsg = us.db.Find(&convInv).Error
	return convInv, resmsg
}

func (us *conventionalModel) CreateProductiveInvoice(prodInv classes.Productiveinvoices) (prodInvObj classes.Productiveinvoices, resmsg error) {
	resmsg = us.db.Create(prodInv).Error
	return prodInvObj, resmsg
}

func (us *conventionalModel) FindAllProductiveInvoice() (prodInv []classes.Productiveinvoices, resmsg error) {
	resmsg = us.db.Find(&prodInv).Error
	return prodInv, resmsg
}
