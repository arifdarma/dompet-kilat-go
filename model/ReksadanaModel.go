package model

import (
	"simpleweb/classes"

	"gorm.io/gorm"
)

type ReksadanaModel interface {
	Create(reksadana classes.Reksadanas) (reksadanaObj classes.Reksadanas, resmsg error)
	FindAll() (reksadana []classes.Reksadanas, resmsg error)
}

type reksadanaModel struct {
	db *gorm.DB
}

func NewReksadanasModel(db *gorm.DB) *reksadanaModel {
	return &reksadanaModel{db}
}

func (us *reksadanaModel) Create(reksadana classes.Reksadanas) (reksadanaObj classes.Reksadanas, resmsg error) {
	resmsg = us.db.Create(&reksadana).Error

	return reksadanaObj, resmsg
}

func (us *reksadanaModel) FindAll() (reksadana []classes.Reksadanas, resmsg error) {
	resmsg = us.db.Find(&reksadana).Error
	return reksadana, resmsg
}
