package model

import (
	"simpleweb/classes"

	"gorm.io/gorm"
)

type UserModel interface {
	Register(user classes.Users) (userObj classes.Users, resmsg error)
	Login(userReq classes.Userrequest) (userObj classes.Users, resmsg error)
	SaveToken(userReq classes.Userrequest) (userObj classes.Users, resmsg error)
}

type userModel struct {
	db *gorm.DB
}

func NewUsersModel(db *gorm.DB) *userModel {
	return &userModel{db}
}

func (us *userModel) Register(user classes.Users) (userObj classes.Users, resmsg error) {
	resmsg = us.db.Create(&user).Error
	return userObj, resmsg
}

func (us *userModel) Login(userReq classes.Userrequest) (userObj classes.Users, resmsg error) {
	resmsg = us.db.Where("email =?", userReq.Email).First(&userObj).Error
	return userObj, resmsg
}

func (us *userModel) SaveToken(userReq classes.Userrequest) (userObj classes.Users, resmsg error) {
	resmsg = us.db.Raw("UPDATE users SET token=? WHERE email=?", userReq.Token, userReq.Email).Scan(&userReq).Error
	return userObj, resmsg
}
