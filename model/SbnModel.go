package model

import (
	"simpleweb/classes"

	"gorm.io/gorm"
)

type SbnModel interface {
	Create(sbn classes.Sbns) (sbnObj classes.Sbns, resmsg error)
	FindAll() (sbn []classes.Sbns, resmsg error)
}

type sbnModel struct {
	db *gorm.DB
}

func NewSbnsModel(db *gorm.DB) *sbnModel {
	return &sbnModel{db}
}

func (us *sbnModel) Create(sbn classes.Sbns) (sbnObj classes.Sbns, resmsg error) {
	resmsg = us.db.Create(&sbn).Error
	return sbnObj, resmsg
}

func (us *sbnModel) FindAll() (sbn []classes.Sbns, resmsg error) {
	resmsg = us.db.Find(&sbn).Error
	return sbn, resmsg
}
