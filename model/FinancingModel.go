package model

import (
	"simpleweb/classes"

	"gorm.io/gorm"
)

type FinancingModel interface {
	Create(financing classes.Financings) (financingObj classes.Financings, resmsg error)
	FindAll() (financing []classes.Financings, resmsg error)
}

type financingModel struct {
	db *gorm.DB
}

func NewFinancingsModel(db *gorm.DB) *financingModel {
	return &financingModel{db}
}

func (us *financingModel) Create(financing classes.Financings) (financingObj classes.Financings, resmsg error) {
	resmsg = us.db.Create(&financing).Error
	return financing, resmsg
}

func (us *financingModel) FindAll() (financing []classes.Financings, resmsg error) {
	resmsg = us.db.Find(&financing).Error
	return financing, resmsg
}
