package classes

type Users struct {
	Username string
	Email    string
	Password string
	Token    string
}
