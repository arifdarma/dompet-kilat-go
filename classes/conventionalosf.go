package classes

type Conventionalosfs struct {
	Name   string
	Amount string
	Tenor  int
	Grade  string
	Rate   int
}
