package classes

type Sbns struct {
	Name   string
	Amount string
	Tenor  int
	Rate   int
	Type   string
}
