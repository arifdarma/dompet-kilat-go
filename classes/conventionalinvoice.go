package classes

type Conventionalinvoices struct {
	Name   string
	Amount string
	Tenor  int
	Grade  string
	Rate   int
}
