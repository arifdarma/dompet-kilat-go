package router

import (
	"log"
	"simpleweb/controller"
	"simpleweb/model"
	"simpleweb/service"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Router() {
	dsn := "simpleweb_user:simpleweb_password@tcp(mysql-simpleweb)/simpleweb_db?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "SERVER IS RUNNING",
		})
	})
	reksadanaModel := model.NewReksadanasModel(db)
	reksadanaService := service.NewReksadanasService(reksadanaModel)
	reksadanaController := controller.NewReksadanasController(reksadanaService)

	sbnModel := model.NewSbnsModel(db)
	sbnService := service.NewSbnsService(sbnModel)
	sbnController := controller.NewSbnsController(sbnService)

	financingModel := model.NewFinancingsModel(db)
	financingService := service.NewFinancingsService(financingModel)
	financingController := controller.NewFinancingsController(financingService)

	conventionalModel := model.NewConventionalsModel(db)
	conventionalService := service.NewConventionalsService(conventionalModel)
	conventionalController := controller.NewConventionalsController(conventionalService)

	userModel := model.NewUsersModel(db)
	userService := service.NewUsersService(userModel)
	userController := controller.NewUsersController(userService)

	router.GET("/getConvOsfs", conventionalController.FindAllConventionalOSFs)
	router.GET("/getConvInvs", conventionalController.FindAllConventionalInvoices)
	router.GET("/getProdInvs", conventionalController.FindAllProductiveInvoices)
	router.GET("/getFinancings", financingController.FindAllFinancings)
	router.GET("/getReksadanas", reksadanaController.FindAllReksadanas)
	router.GET("/getSbns", sbnController.FindAllSbns)
	router.POST("/createReksadanas", reksadanaController.CreateReksadanas)
	router.POST("/createSbns", sbnController.CreateSbns)
	router.POST("/createFinancings", financingController.CreateFinancings)
	router.POST("/createConvOsfs", conventionalController.CreateConventionalOsfs)
	router.POST("/createConvInvs", conventionalController.CreateConventionalInvoices)
	router.POST("/createProdInvs", conventionalController.CreateProductiveInvoices)
	router.POST("/register", userController.Register)
	router.POST("/login", userController.Login)
	router.Run()
}
