package service

import (
	"simpleweb/classes"
	"simpleweb/model"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type UserService interface {
	Register(user classes.Users) (userObj classes.Users, resmsg error)
	Login(userReq classes.Userrequest) (userObj classes.Users, resmsg error)
	GenerateJWT(userReq classes.Userrequest) (string, error)
}

type userService struct {
	model model.UserModel
}

func NewUsersService(model model.UserModel) *userService {
	return &userService{model}
}

func (us *userService) Register(user classes.Users) (userObj classes.Users, resmsg error) {
	userObj, resmsg = us.model.Register(user)
	return userObj, resmsg
}

func (us *userService) Login(userReq classes.Userrequest) (userObj classes.Users, resmsg error) {
	userObj, resmsg = us.model.Login(userReq)
	return userObj, resmsg
}

func (us *userService) GenerateJWT(userReq classes.Userrequest) (string, error) {
	var mySigningkey = []byte("simplewebkey")
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorize"] = true
	claims["user"] = userReq.Email
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(mySigningkey)
	if err != nil {
		return "", err
	}
	userReq.Token = tokenString
	userObj, err := us.model.SaveToken(userReq)
	return userObj.Email, err
}
