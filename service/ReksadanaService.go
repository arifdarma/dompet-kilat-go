package service

import (
	"simpleweb/classes"
	"simpleweb/model"
)

type ReksadanaService interface {
	Create(reksadana classes.Reksadanas) (reksadanaObj classes.Reksadanas, resmsg error)
	FindAll() (reksadana []classes.Reksadanas, resmsg error)
}

type reksadanaService struct {
	model model.ReksadanaModel
}

func NewReksadanasService(model model.ReksadanaModel) *reksadanaService {
	return &reksadanaService{model}
}

func (us *reksadanaService) Create(reksadana classes.Reksadanas) (reksadanaObj classes.Reksadanas, resmsg error) {
	reksadanaObj, resmsg = us.model.Create(reksadana)
	return reksadanaObj, resmsg
}

func (us *reksadanaService) FindAll() (reksadana []classes.Reksadanas, resmsg error) {
	reksadana, resmsg = us.model.FindAll()
	return reksadana, resmsg
}
