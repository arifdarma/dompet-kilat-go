package service

import (
	"simpleweb/classes"
	"simpleweb/model"
)

type FinancingService interface {
	Create(financing classes.Financings) (financingObj classes.Financings, resmsg error)
	FindAll() (financing []classes.Financings, resmsg error)
}

type financingService struct {
	model model.FinancingModel
}

func NewFinancingsService(model model.FinancingModel) *financingService {
	return &financingService{model}
}

func (us *financingService) Create(financing classes.Financings) (financingObj classes.Financings, resmsg error) {
	financingObj, resmsg = us.model.Create(financing)
	return financingObj, resmsg
}

func (us *financingService) FindAll() (financing []classes.Financings, resmsg error) {
	financing, resmsg = us.model.FindAll()
	return financing, resmsg
}
