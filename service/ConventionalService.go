package service

import (
	"simpleweb/classes"
	"simpleweb/model"
)

type ConventionalService interface {
	CreateConventionalOSF(convOsf classes.Conventionalosfs) (convOsfObj classes.Conventionalosfs, resmsg error)
	FindAllConventionalOSF() (convOsf []classes.Conventionalosfs, resmsg error)
	CreateConventionalInvoice(convInv classes.Conventionalinvoices) (convInvObj classes.Conventionalinvoices, resmsg error)
	FindAllConventionalInvoice() (convInv []classes.Conventionalinvoices, resmsg error)
	CreateProductiveInvoice(prodInv classes.Productiveinvoices) (prodInvObj classes.Productiveinvoices, resmsg error)
	FindAllProductiveInvoice() (prodInv []classes.Productiveinvoices, resmsg error)
}

type conventionalService struct {
	model model.ConventionalModel
}

func NewConventionalsService(model model.ConventionalModel) *conventionalService {
	return &conventionalService{model}
}

func (us *conventionalService) CreateConventionalOSF(convOsf classes.Conventionalosfs) (convOsfObj classes.Conventionalosfs, resmsg error) {
	convOsfObj, resmsg = us.model.CreateConventionalOSF(convOsf)
	return convOsfObj, resmsg
}

func (us *conventionalService) FindAllConventionalOSF() (convOsf []classes.Conventionalosfs, resmsg error) {
	convOsf, resmsg = us.model.FindAllConventionalOSF()
	return convOsf, resmsg
}

func (us *conventionalService) CreateConventionalInvoice(convInv classes.Conventionalinvoices) (convInvObj classes.Conventionalinvoices, resmsg error) {
	convInvObj, resmsg = us.model.CreateConventionalInvoice(convInv)
	return convInvObj, resmsg
}

func (us *conventionalService) FindAllConventionalInvoice() (convInv []classes.Conventionalinvoices, resmsg error) {
	convInv, resmsg = us.model.FindAllConventionalInvoice()
	return convInv, resmsg
}

func (us *conventionalService) CreateProductiveInvoice(prodInv classes.Productiveinvoices) (prodInvObj classes.Productiveinvoices, resmsg error) {
	prodInvObj, resmsg = us.model.CreateProductiveInvoice(prodInv)
	return prodInvObj, resmsg
}

func (us *conventionalService) FindAllProductiveInvoice() (prodInv []classes.Productiveinvoices, resmsg error) {
	prodInv, resmsg = us.model.FindAllProductiveInvoice()
	return prodInv, resmsg
}
