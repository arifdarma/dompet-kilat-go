package service

import (
	"simpleweb/classes"
	"simpleweb/model"
)

type SbnService interface {
	Create(sbn classes.Sbns) (sbnObj classes.Sbns, resmsg error)
	FindAll() (sbn []classes.Sbns, resmsg error)
}

type sbnService struct {
	model model.SbnModel
}

func NewSbnsService(model model.SbnModel) *sbnService {
	return &sbnService{model}
}

func (us *sbnService) Create(sbn classes.Sbns) (sbnObj classes.Sbns, resmsg error) {
	sbnObj, resmsg = us.model.Create(sbn)
	return sbnObj, resmsg
}

func (us *sbnService) FindAll() (sbn []classes.Sbns, resmsg error) {
	sbn, resmsg = us.model.FindAll()
	return sbn, resmsg
}
